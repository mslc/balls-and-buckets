// gs = Google Script
var gsURL = "https://script.google.com/macros/s/AKfycbzpEkjAsLvZ5pBswGypkSA1XJ1XP-BQ2rnYDCAaQCo_ugE-kY0/exec";  // Deployed version
////// TODO: Why isn't the latest deployed version working??
//var gsURL = "https://script.google.com/macros/s/AKfycbxyX3nMxSx0PUlykh0ZM7tehQYi981hSJaPw9eUG4s/dev";   // Dev version

function runSetup() {
  $("#start").hide();
  $("#loading").show();
  
  var name = $("#classname").val();
  
  var email = $("#autoemail").prop("checked");
  
  var gsDebug = $("#debugmode").prop("checked");
  var gsLive = !($("#dummymode").prop("checked"));
  
  var requestURL = gsURL + "?name=" + escape(name) + (email ? "&email=false" : "") + (gsDebug ? "&debug=true" : "") + (gsLive ? "&live=true" : "");

//  console.log( "Query URL: " + requestURL );
  window.location = requestURL;
}

function reset() {
  $("#loading").hide();
  $("#start").show();
};

$(window).on("pageshow", reset);


/* jQuery code to slide in/out FAQ answers by clicking question title */
function doSlide( node ) {
  /* Show/hide all elements between clicked element and the next <li> element */
  $(node).nextUntil(".question").slideToggle();
}

var qs = $("#faqs > .question");

/* Set onclick handler */
qs.click(function(){doSlide(this);})

/* Hide answers initially */
qs.nextUntil(".question").hide();

$("#clicktoshow").show();

function showdebug() {
  document.getElementById("showdebug").style.display = "none";
  document.getElementById("debug").style.display = "";
}
